# Imagen raíz
FROM node

# Carpeta raíz
WORKDIR /apitechu

# Copia de archivos de proyecto
ADD . /apitechu

# Instalo paquetes necesarios
RUN npm install --only-prod

# Puerto que se expone
EXPOSE 3000

# Comando de iniciaización
CMD ["node", "server.js"]
