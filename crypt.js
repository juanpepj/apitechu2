const bcrypt = require('bcrypt');

function hash(data) {
  console.log("hashing data");

  //pasadas que hará el encriptador para hasear, recomendable 10
  return bcrypt.hashSync(data, 10);
}

function checkPassword(passwordFromUserInPlainText, passwordFromDBHashed) {
 console.log("Checking password");

 return bcrypt.compareSync(passwordFromUserInPlainText, passwordFromDBHashed);
}

module.exports.hash = hash;
module.exports.checkPassword = checkPassword;
