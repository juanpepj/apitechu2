const io = require('../io');
const crypt = require('../crypt');

const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujppj13ed/collections/";
const mLabAPIKEY = "apiKey=" + process.env.MLAB_API_KEY;


function getAccountsV1(req, res){
  console.log("GET /apitechu/v1/accounts");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.get("account?" + mLabAPIKEY,
    function(err, resMLab, body) {
      var response = !err ? body : {
        "msg" : "Error obteniendo cuentas"
      }
      res.send(response);
    }
  );
}

function getAccountByUserIdV1(req, res){
  console.log("GET /apitechu/v1/accounts/:user_id");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  //procesamos el id para evitar la inyección SQL
  var user_id = Number.parseInt(req.params.user_id);
  console.log("El id del cuenta a buscar es "+ user_id);
  var query = "q=" + JSON.stringify({"user_id": user_id });
  console.log("Query es "+ query);

  httpClient.get("account?" + query + "&" + mLabAPIKEY,
    function(err, resMLab, body) {
      if (err){
        var response = {
          "msg" : "Error obteniendo cuenta"
        }
        res.status(500);
      } else {
        if (body.length > 0){
          var response = body;
        } else {
          var response = {
            "msg" : "Cuenta no encontrada"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  );
}

module.exports.getAccountsV1 = getAccountsV1;
module.exports.getAccountByUserIdV1 = getAccountByUserIdV1;
