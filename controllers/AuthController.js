const io = require('../io');
const crypt = require('../crypt');

const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujppj13ed/collections/";
const mLabAPIKEY = "apiKey=" + process.env.MLAB_API_KEY;

function loginUserV1(req, res){
  console.log("POST /apitechu/v1/login");

  var users = require('../usuarios.json');
  var result = { "msg" : "Login incorrecto" };

  const index = users.findIndex(users => users.email == req.body.email);
  if (index>-1){
    console.log("Usuario encontrado en la posición "+ index);
    var searchUser = users[index];

    //si el usuario ya estaba logado pero la password es incorrecta le hago logout
    // delete searchUser.logged;

    console.log(searchUser);
    if (searchUser.password == req.body.password){
      console.log("Password correcta, escribimos en el fichero ");
      searchUser.logged = true;
      io.writeUserDataToFile(users);
      result = { "msg" : "Login correcto", "idUsuario" : searchUser.id };
    } else {
      console.log("Password incorrecta");
    }
  }

  res.send(result);

}

function loginUserV2(req, res){
  console.log("POST /apitechu/v2/login");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  console.log("El email del usuario a buscar es "+ req.body.email);
  var query = "q=" + JSON.stringify({"email":req.body.email});
  console.log("Query es "+ query);

  httpClient.get("user?" + query + "&" + mLabAPIKEY,
    function(errGet, resMLabGet, bodyGet) {
      var response = {
        "msg" : "Login incorrecto"
      }
      if (errGet){
        res.status(500).send(response);
      } else {
        if (bodyGet.length && crypt.checkPassword(req.body.password,bodyGet[0].password)){

          console.log("El id del usuario a buscar es "+ bodyGet[0].id);
          var query = "q=" + JSON.stringify({"id":bodyGet[0].id});
          console.log("Query es "+ query);

          httpClient.put("user?" + query + "&" + mLabAPIKEY, {"$set":{"logged":true}},
            function(errPut, resMLabPut, bodyPut) {
              if (errPut){
                console.log(errPut);
                res.status(500).send(response);
              } else {
                console.log("Usuario actualizado en Mlab");
                response = {
                  "msg" : "Login correcto",
                  "idUsuario" : bodyGet[0].id
                }
                res.send(response);
              }
            }
          )
        } else {
          console.log("Login incorrecto");
          res.status(401).send(response);
        }
      }
    }
  );

}

function logoutUserV1(req, res){
  console.log("POST /apitechu/v1/logout/:id");
  console.log("El id del usuario a hacer logout es "+ req.params.id);

  var users = require('../usuarios.json');
  var result = { "msg" : "Logout incorrecto" };

  const index = users.findIndex(users => users.id == req.params.id);
  if (index>-1){
    console.log("Usuario encontrado en la posición "+ index);
    var searchUser = users[index];
    console.log(searchUser);
    if (searchUser.logged == true){
      delete searchUser.logged;
      io.writeUserDataToFile(users);
      result = { "msg" : "Logout correcto", "idUsuario" : searchUser.id };
    }
  }
  res.send(result);
}

function logoutUserV2(req, res){
  console.log("POST /apitechu/v1/logout/:id");
  console.log("El id del usuario a hacer logout es "+ req.params.id);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  //procesamos el id para evitar la inyección SQL
  var id = Number.parseInt(req.params.id);
  console.log("El id del usuario a buscar es "+ id);
  var query = "q=" + JSON.stringify({"id":id, "logged":true});
  console.log("Query es "+ query);

  httpClient.put("user?" + query + "&" + mLabAPIKEY, {"$unset":{"logged":""}},
    function(errPut, resMLabPut, bodyPut) {
      var response = {
        "msg" : "Logout incorrecto"
      }
      res.status(403);
      if (errPut){
        res.status(500);
      } else {
        if (bodyPut.n > 0){
          console.log("Usuario actualizado en Mlab");
          var response = {
            "msg" : "Logout correcto",
             "idUsuario" : id
          }
          res.status(200);
        } else {
          console.log("Usuario no encontrado");
        }
      }
      res.send(response);
    }
  )
}


module.exports.loginUserV1 = loginUserV1;
module.exports.loginUserV2 = loginUserV2;
module.exports.logoutUserV1 = logoutUserV1;
module.exports.logoutUserV2 = logoutUserV2;
