const io = require('../io');
const crypt = require('../crypt');

const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujppj13ed/collections/";
const mLabAPIKEY = "apiKey=" + process.env.MLAB_API_KEY;


function getUsersV1(req, res){
  console.log("GET /apitechu/v1/users");

  //envia el fichero entero
  //res.sendFile('usuarios.json', {root: __dirname})

  var users = require('../usuarios.json');
  var result = {};

  var count = req.query.$count;
  if (count && count == "true"){
    console.log("recibido parámetro $count vale: " + count);
    result.count = users.length;
  }

  var top = req.query.$top;
  if (top){
    console.log("recibido parámetro $top vale: " + top);
    users = users.slice(0, top);
  }
  result.users = users;

  res.send(result);
}

function getUsersV2(req, res){
  console.log("GET /apitechu/v2/users");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.get("user?" + mLabAPIKEY,
    function(err, resMLab, body) {
      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios"
      }
      res.send(response);
    }
  );
}

function getUserByIdV2(req, res){
  console.log("GET /apitechu/v2/users/:id");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  //procesamos el id para evitar la inyección SQL
  var id = Number.parseInt(req.params.id);
  console.log("El id del usuario a buscar es "+ id);
  var query = "q=" + JSON.stringify({"id":id});
  console.log("Query es "+ query);

  httpClient.get("user?" + query + "&" + mLabAPIKEY,
    function(err, resMLab, body) {
      if (err){
        var response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
      } else {
        if (body.length > 0){
          var response = body[0];
        } else {
          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  );
}

function createUserV1(req, res){
  console.log("POST /apitechu/v1/users");

  var users = require('../usuarios.json');

  var newUser = {
    "first_name":req.body.first_name,
    "last_name":req.body.last_name,
    "email":req.body.email
  }
  console.log(newUser);

  users.push(newUser);
  console.log("Usuario añadido al array");

  io.writeUserDataToFile(users);
}

function createUserV2(req, res){
  console.log("POST /apitechu/v2/users");
  console.log(req.body.id);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser = {
    "id":req.body.id,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password": crypt.hash(req.body.password)
  }

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.post("user?" + mLabAPIKEY, newUser,
    function(err, resMLab, body) {
      console.log("Usuario creado en Mlab");
      res.status(201).send({"msg":"Usuario creado"});
    }
  )


}

function deleteUserV1(req, res){
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("El id del usuario a borrar es "+ req.params.id);

  var users = require('../usuarios.json');

  // Bucle con for
  // for(var i = 0; i < users.length;i++){
  //    if (users[i].id == req.params.id){
  //      console.log("Elementos encontrado en la posición "+ i);
  //      var elementoEliminado = users.splice(i, 1);
  //      console.log("Elementos eliminados: ")
  //      console.log(elementoEliminado);
  //      break;
  //    }
  // }

  // Bucle con for in
  // for (var i in users) {
  //   if (users[i].id == req.params.id){
  //     console.log("Elementos encontrado en la posición "+ i);
  //     var elementoEliminado = users.splice(i, 1);
  //     console.log("Elementos eliminados: ")
  //     console.log(elementoEliminado);
  //     break;
  //   }
  // }

  // Bucle con for each
  // users.forEach(function(user, index) {
  //   if (users[index].id == req.params.id){
  //     console.log("Elementos encontrado en la posición "+ index);
  //     var elementoEliminado = users.splice(index, 1);
  //     console.log("Elementos eliminados: ")
  //     console.log(elementoEliminado);
  //     break;
  //   }
  // });

  // Bucle con for of
  // var result = [] ;
  // for (let user of users) {
  //   if (user.id != req.params.id){
  //     console.log("Añado el usuario a la lista final")
  //     console.log(user);
  //     result.push(user);
  //   }
  // }
  // users = result;


  // Bucle con for of y destucturing
  // for (var [index,user] of users.entries()) {
  //   if (user.id == req.params.id){
  //     console.log("Elementos encontrado en la posición "+ index);
  //     var elementoEliminado = users.splice(index, 1);
  //     console.log("Elemento eliminado: ")
  //     console.log(elementoEliminado);
  //     break;
  //   }
  //   console.log();
  // }


  // Bucle con find Index
  const index = users.findIndex(users => users.id == req.params.id);
  if (index>-1){
    console.log("Elementos encontrado en la posición "+ index);
    var elementoEliminado = users.splice(index, 1);
    console.log("Elementos eliminados: ")
    console.log(elementoEliminado);
  }

  io.writeUserDataToFile(users);
}


function deleteUserV2(req, res){
  console.log("DELETE /apitechu/v2/users/:id");
  console.log("El id del usuario a borrar es "+ req.params.id);

  //procesamos el id para evitar la inyección SQL
  var id = Number.parseInt(req.params.id);
  console.log("El id del usuario a buscar es "+ id);
  var query = "q=" + JSON.stringify({"id":id});
  console.log("Query es "+ query);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.put("user?" + query + "&" + mLabAPIKEY, [{}],
    function(err, resMLab, body) {
      if (err){
        var response = {
          "msg" : "Error borrando usuario"
        }
        res.status(500);
      } else {
        if (body.removed > 0){
          var response = {
            "msg" : "Usuario borrado"
          }
        } else {
          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  )

}

module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV1 = createUserV1;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUserV1 = deleteUserV1;
module.exports.deleteUserV2 = deleteUserV2;
