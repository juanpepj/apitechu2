require('dotenv').config();
const express = require('express');
const app = express(); //arrancamos el framework de express

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");

 res.set("Access-Control-Allow-Headers", "Content-Type");
 next();
}



const usersController = require('./controllers/UsersController.js');
const authController = require('./controllers/AuthController.js');
const accountsController = require('./controllers/AccountsController.js');

app.use(express.json()); //se indica que por defecto lo que llega en el body lo intenta procesar como json
app.use(enableCORS); //se habilita el crossDomain

const port = process.env.PORT || 3000; //si la varialbe no existe uso el puerto 3000 por defecto
app.listen(port);
console.log("API escuchando en el puerto " + port);

app.get("/apitechu/v1/hello",
  function(req, res){
    console.log("GET /apitechu/v1/hello");
    res.send({"msg":"Hola desde API TechU"});
  }
)

app.get("/apitechu/v1/users", usersController.getUsersV1);
app.get("/apitechu/v2/users", usersController.getUsersV2);
app.get("/apitechu/v2/users/:id", usersController.getUserByIdV2);

app.post("/apitechu/v1/users", usersController.createUserV1);
app.post("/apitechu/v2/users", usersController.createUserV2);

app.delete("/apitechu/v1/users/:id", usersController.deleteUserV1);
app.delete("/apitechu/v2/users/:id", usersController.deleteUserV2);


app.post("/apitechu/v1/login", authController.loginUserV1);
app.post("/apitechu/v2/login", authController.loginUserV2);
app.post("/apitechu/v1/logout/:id", authController.logoutUserV1);
app.post("/apitechu/v2/logout/:id", authController.logoutUserV2);

app.get("/apitechu/v1/accounts", accountsController.getAccountsV1);
app.get("/apitechu/v1/accounts/:user_id", accountsController.getAccountByUserIdV1);




app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req,res){
    console.log("Parámetros");
    console.log(req.params);

    console.log("Query string");
    console.log(req.query)

    console.log("Headers");
    console.log(req.headers)

    console.log("Body");
    console.log(req.body)
  }
)
